# Scheduler task

### Binary index implementation:

We just have nodes with datetime timestamp as a key, in binary tree we can traverse
thou nodes with ease. First off all just using binary search find the first
node that matches our needs, after that just move to the bigger(with bigger timestamp)
nodes while collecting them to a bucket.
 
### Running project

```bash
# to run the project execute commands in your terminal
cd path/to/project
composer install
cd publish
php -S localhost::8080
```

### Running tests

Actually there are very few tests but anyway...
```bash
cd path/to/project
./vendor/bin/phpunit
```

### Api description

GET /api/v1/events - get upcoming events

GET /api/v1/events/{id} - get event by id

POST /api/v1/events - create event
```
form-data params:

source     - string, source name // as I understend third party service
name       - string, name of the event
published  - RFC3339, date time, as I understand it is a date when event will take place
registered - bool, is event registered in our service? hope its correct :)
```

PUT /api/v1/events/{id} - update event
```
form-data params:

published  - RFC3339, date time, date when event will take place
registered - bool, is event registered in our service
```

# Sorry for docker/vagrant is not included
