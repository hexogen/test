<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class EventTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_get_upcoming_events()
    {
        $response = $this->get('/api/v1/events');

        $response->assertResponseStatus(200);

        $response->seeJsonContains(['status' => 'success']);
    }

    /**
     * @test
     */
    public function it_should_create_event()
    {
        $response = $this->post('/api/v1/events', [
            'source' => 'source_1',
            'name' => 'name_1',
            'published' => '2020-01-05T15:52:01+00:00',
            'registered' => true,
        ]);

        $response->assertResponseStatus(200);

        $response->seeJsonContains(['status' => 'success']);

        json_decode($this->response->getContent());
    }
}
