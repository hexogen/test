<?php

namespace App\Http\Controllers;

use App\Entities\EventEntity;
use App\Repositories\EventRepository;
use DateTimeInterface;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * @var EventRepository
     */
    private $eventRepository;

    /**
     * Create a new controller instance.
     *
     * @param EventRepository $eventRepository
     */
    public function __construct(EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    public function index()
    {
        $entitiesFiles = scandir(storage_path('index'), 1);

        return response()->json([
            'status' => 'success',
            'data' => $this->eventRepository->getUpcoming(10),
        ]);
    }

    public function show(string $id)
    {
        $event = $this->eventRepository->getById($id);

        return response()->json([
            'status' => 'success',
            'data' => [
                'id' => $event->id,
                'source' => $event->source,
                'name' => $event->name,
                'create' => $event->created->format(DateTimeInterface::RFC3339),
                'published' => $event->published->format(DateTimeInterface::RFC3339),
            ],
        ]);
    }

    /**
     * @param Request $request
     * @param string $source
     * @param string $name
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function register(Request $request)
    {
        try {
            $entityData = $this->getEntityData($request);
            $entityData['id'] = null;
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'wrong date time format, use: RFC3339',
            ]);
        }

        if ($this->eventRepository->getByName($entityData['source'], $entityData['name']) == null) {
            $eventEntity = app()->make(EventEntity::class, $entityData);
        } else {
            throw new \Exception("entity already exists");
        }

        $eventEntity = $this->eventRepository->persist($eventEntity);

        return response()->json([
            'status' => 'success',
            'message' => 'event added to our service',
            'data' => [
                'id' => $eventEntity->id,
                'source' => $eventEntity->source,
                'name' => $eventEntity->name,
                'registered' => $eventEntity->registered,
                'create' => $eventEntity->created->format(DateTimeInterface::RFC3339),
                'published' => $eventEntity->published->format(DateTimeInterface::RFC3339),
            ],
        ]);
    }

    /**
     * @param Request $request
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, string $id)
    {
        $eventEntity = $this->eventRepository->getById($id);

        try {
            $entityData = $this->getEntityData($request);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'wrong date time format, use: RFC3339',
            ]);
        }

        if ($eventEntity == null) {
            throw new \Exception("Entity is not created yat");
        }

        $eventEntity->registered = $entityData['registered'];
        $eventEntity->published = $entityData['published'];

        $eventEntity = $this->eventRepository->persist($eventEntity);

        return response()->json([
            'status' => 'success',
            'message' => 'event added to our service',
            'data' => [
                'id' => $eventEntity->id,
                'source' => $eventEntity->source,
                'name' => $eventEntity->name,
                'registered' => $eventEntity->registered,
                'create' => $eventEntity->created->format(DateTimeInterface::RFC3339),
                'published' => $eventEntity->published->format(DateTimeInterface::RFC3339),
            ],
        ]);
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function getEntityData(Request $request): array
    {
        $entityData = [
            'name' => $request->post('name'),
            'source' => $request->post('source'),
            'published' => new \DateTime($request->post('published')),
            'registered' => (bool)$request->post('registered', false),
        ];
        return $entityData;
    }
}
