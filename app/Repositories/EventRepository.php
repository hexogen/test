<?php


namespace App\Repositories;


use App\Entities\EventEntity;
use App\Events\EventPersisted;

class EventRepository
{
    /**
     * @param string $id
     * @return EventEntity|null
     * @throws \Exception
     */
    public function getById(string $id): ?EventEntity
    {
        $path = storage_path('index') . DIRECTORY_SEPARATOR . $id;

        if (!file_exists($path)) {
            return null;
        }

        $fp = fopen($path, 'r');

        if (flock($fp, LOCK_SH)) {
            $entityData = fread($fp, filesize($path));
            $entity = unserialize($entityData);
            flock($fp, LOCK_UN);
            fclose($fp);
            return $entity;
        }
        fclose($fp);
        throw new \Exception("can not get access to a file"); // TODO create exception and handle it
    }

    /**
     * @param string $source
     * @param string $name
     * @return EventEntity|null
     * @throws \Exception
     */
    public function getByName(string $source, string $name): ?EventEntity
    {
        $path = $this->getPath($source, $name);

        return $this->getById($path);
    }

    public function getUpcoming(int $length, int $offset = 0): array
    {
        $entities = [];
        $entitiesFiles = scandir(storage_path('index'));
        $entitiesFiles = array_diff($entitiesFiles, ['.', '..']);

        foreach ($entitiesFiles as $entitiesFile) {
            $entity = unserialize(file_get_contents(storage_path('index') . DIRECTORY_SEPARATOR . $entitiesFile));
            $entities[] = $entity;
        }

        usort($entities, function (EventEntity $a, EventEntity $b) {
            return $a->published <=> $b->published;
        });

        return array_slice($entities, $offset, $length); // TODO check for errors
    }

    /**
     * @param EventEntity $eventEntity
     * @return EventEntity
     * @throws \Exception
     */
    public function persist(EventEntity $eventEntity)
    {
        $this->createDir($eventEntity->source);
        $path = $this->getPath($eventEntity->source, $eventEntity->name);

        // if event is not already created
        if ($eventEntity->id == null) {
            $eventEntity->id = $this->getId($eventEntity->source, $eventEntity->name);
            $eventEntity->created = new \DateTime();
            $oldEvent = null;
        } else {
            $oldEvent = $this->getById($eventEntity->id);
        }

        $entityData = serialize($eventEntity);

        $fp = fopen($path, 'c');

        if (flock($fp, LOCK_EX)) {
            ftruncate($fp, 0);// truncate file
            fwrite($fp, $entityData);
            flock($fp, LOCK_UN);
            fclose($fp);
            event(new EventPersisted($eventEntity, $oldEvent));//fire EventPersisted event
            // :( sorry for not finding good synonym for event word
        } else {
            fclose($fp);
            throw new \Exception("can not get access to a file"); // TODO create exception and handle it
        }

        return $eventEntity;
    }

    /**
     * @param string $source
     * @param string $name
     * @return string
     */
    protected function getPath(string $source, string $name): string
    {
        $path = $this->getDir($source) . DIRECTORY_SEPARATOR . md5($name);
        // some sort of hash table simulation index :)
        // additionally in modern languages there ara already multi treads implementations of data structures that are
        // handles concurrency headache for us
        return $path;
    }

    /**
     * @param string $source
     * @return string
     */
    protected function getDir(string $source): string
    {
        $dir = storage_path('events' . DIRECTORY_SEPARATOR . md5($source));
        return $dir;
    }

    /**
     * @param string $source
     */
    protected function createDir(string $source)
    {
        $dir = $this->getDir($source);
        if (!is_dir($dir)) {
            mkdir($dir);
        }
    }

    protected function getId(string $source, string $name): string
    {
        return md5($source . DIRECTORY_SEPARATOR . $name);
    }
}
