<?php

namespace App\Entities;

class EventEntity
{
    //TODO make fields protected and create getters and setters
    /**
     * @var string|null
     */
    public $id;
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $source;
    /**
     * @var bool
     */
    public $registered;
    /**
     * @var \DateTime
     */
    public $published;
    /**
     * @var \DateTime|null
     */
    public $created;

    /**
     * EventEntity constructor.
     * @param string|null $id
     * @param string $name
     * @param string $source
     * @param bool $registered
     * @param \DateTime $published
     * @param \DateTime|null $created
     */
    public function __construct(
        ?string $id,
        string $name,
        string $source,
        bool $registered,
        \DateTime $published,
        ?\DateTime $created = null
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->source = $source;
        $this->registered = $registered;
        $this->published = $published;
        $this->created = $created;
    }
}
