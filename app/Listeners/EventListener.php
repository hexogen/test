<?php

namespace App\Listeners;

use App\Events\EventPersisted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class EventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param EventPersisted $event
     * @return void
     */
    public function handle(EventPersisted $event)
    {
        $entity = $event->entity;
        $oldEntity = $event->oldEntity;

        if ($oldEntity) {
            // we need it to properly reindex events by data
            // to find it and delete in a tree by old publish date
            // and add again to with new publish date as a key
        }

        // TODO remove from index if event is marked as unregistered

        $key = $this->getFilePath($entity->id);
        file_put_contents($key, serialize($entity));
    }

    protected function getFilePath($id)
    {
        return storage_path('index' . DIRECTORY_SEPARATOR . $id);
    }
}
