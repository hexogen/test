<?php

namespace App\Providers;

use App\Events\EventPersisted;
use App\Listeners\EventListener;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        EventPersisted::class => [
            EventListener::class,
        ],
    ];
}
