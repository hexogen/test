<?php

namespace App\Events;

use App\Entities\EventEntity;

class EventPersisted extends Event
{
    /**
     * @var EventEntity
     */
    public $entity;
    /**
     * @var EventEntity|null
     */
    public $oldEntity;

    /**
     * Create a new event instance.
     *
     * @param EventEntity $entity
     * @param EventEntity|null $oldEntity
     */
    public function __construct(EventEntity $entity, ?EventEntity $oldEntity)
    {
        $this->entity = $entity;
        $this->oldEntity = $oldEntity;
    }
}
